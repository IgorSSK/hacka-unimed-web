// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing the generated config
var firebaseConfig = {
    apiKey: 'AIzaSyB1Qfgvn029Zjm7H6IMqSkup2KSN7SJmEE',
    authDomain: 'unimed-hackaton-2022.firebaseapp.com',
    projectId: 'unimed-hackaton-2022',
    storageBucket: 'unimed-hackaton-2022.appspot.com',
    messagingSenderId: '988240279893',
    appId: '1:988240279893:web:e126644c6ea748818717dc'
};

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
    console.log('Received background message ', payload);

    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.body
    };

    self.registration.showNotification(notificationTitle, notificationOptions);
});
