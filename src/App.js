import { useSelector } from 'react-redux';

import { ThemeProvider } from '@mui/material/styles';
import { Alert, AlertTitle, CssBaseline, Stack, StyledEngineProvider } from '@mui/material';

// routing
import Routes from 'routes';

// defaultTheme
import themes from 'themes';

// project imports
import NavigationScroll from 'layout/NavigationScroll';
import { useState } from 'react';
import { getToken, onMessageListener } from 'services/firebase';

import Snackbar from '@mui/material/Snackbar';

// ==============================|| APP ||============================== //

const App = () => {
    const customization = useSelector((state) => state.customization);

    const [show, setShow] = useState(false);
    const [notification, setNotification] = useState({ title: '', body: '' });
    const [isTokenFound, setTokenFound] = useState(false);
    getToken(setTokenFound);

    onMessageListener()
        .then((payload) => {
            setShow(true);
            setNotification({ title: payload.notification.title, body: payload.notification.body });
            console.log(payload);
        })
        .catch((err) => console.log('failed: ', err));

    return (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={themes(customization)}>
                <CssBaseline />
                <NavigationScroll>
                    <Snackbar
                        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                        open={show}
                        onClose={() => setShow(!show)}
                        key={Math.random()}
                        autoHideDuration={6000}
                    >
                        <Alert onClose={() => setShow(!show)} severity="info" sx={{ width: '100%' }}>
                            <AlertTitle>{notification.title}</AlertTitle>
                            {notification.body}
                        </Alert>
                    </Snackbar>
                    <Routes />
                </NavigationScroll>
            </ThemeProvider>
        </StyledEngineProvider>
    );
};

export default App;
