// assets
import { IconKey } from '@tabler/icons';

// constant
const icons = {
    IconKey
};

// ==============================|| EXTRA PAGES MENU ITEMS ||============================== //

const pages = {
    id: 'beneficiary',
    title: 'Beneficiário',
    // caption: 'Pages Caption',
    type: 'group',
    children: [
        {
            id: 'basic-informations',
            title: 'Informações Básicas',
            type: 'item',
            icon: icons.IconKey,
            url: '/beneficiary/information'
        }
        // {
        //     id: 'timeline',
        //     title: 'Linha do Tempo',
        //     type: 'item',
        //     icon: icons.IconKey,
        //     url: '/beneficiary/timeline'
        // }
    ]
};

export default pages;
