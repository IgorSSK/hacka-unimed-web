import firebase from 'firebase/app';
import 'firebase/messaging';

var firebaseConfig = {
    apiKey: 'AIzaSyB1Qfgvn029Zjm7H6IMqSkup2KSN7SJmEE',
    authDomain: 'unimed-hackaton-2022.firebaseapp.com',
    projectId: 'unimed-hackaton-2022',
    storageBucket: 'unimed-hackaton-2022.appspot.com',
    messagingSenderId: '988240279893',
    appId: '1:988240279893:web:e126644c6ea748818717dc'
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

export const getToken = (setTokenFound) => {
    return messaging
        .getToken({ vapidKey: 'BE4kFdUVRZoE3qU-8pK6kJoZki-65p0ojP2ycFA677DzjGlZodEjNHIQVE0wUAOyEAHuGP1u93amYZDS_iqG3aQ' })
        .then((currentToken) => {
            if (currentToken) {
                console.log('current token for client: ', currentToken);
                setTokenFound(true);
                // Track the token -> client mapping, by sending to backend server
                // show on the UI that permission is secured
            } else {
                console.log('No registration token available. Request permission to generate one.');
                setTokenFound(false);
                // shows on the UI that permission is required
            }
        })
        .catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            // catch error while creating client token
        });
};

export const onMessageListener = () =>
    new Promise((resolve) => {
        messaging.onMessage((payload) => {
            resolve(payload);
        });
    });
