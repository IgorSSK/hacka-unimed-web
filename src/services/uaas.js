import axios from 'axios';

export default class UaaS {
    static baseUrl = 'https://rhv41rzd8d.execute-api.us-east-1.amazonaws.com/Prod';

    static async getBeneficiaries() {
        const response = await axios.get(`^${this.baseUrl}/beneficiario`);

        return response.data;
    }

    static async getBeneficiary(id) {
        const response = await axios.get(`${this.baseUrl}/beneficiario/${id}`, {
            headers: {
                // 'Access-Control-Allow-Origin': '*'
                // 'Content-type': 'application/json'
            }
        });

        return response.data;
    }
}
