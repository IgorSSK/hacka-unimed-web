import React, { useEffect, useState } from 'react';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import TimelineDot from '@mui/lab/TimelineDot';
import HotelIcon from '@mui/icons-material/Hotel';
import RepeatIcon from '@mui/icons-material/Repeat';
import Typography from '@mui/material/Typography';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import ImageIcon from '@mui/icons-material/Image';
import { PersonSearch, Science, Notifications, FileDownload, Add } from '@mui/icons-material';
import Fab from '@mui/material/Fab';
import { Button, DialogActions, TextField } from '@mui/material';
import { useTheme } from '@mui/styles';

const BeneficiaryTimeline = ({ events }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [isCadOpen, setIsCadOpen] = useState(false);
    const [selected, setSelected] = useState({});
    const [timeline, setTimeline] = useState([]);
    const [newEvent, setNewEvent] = useState();
    const theme = useTheme();
    //     useState([
    //     {
    //         date: '01/02/2022',
    //         icon: RepeatIcon,
    //         color: 'info',
    //         events: [
    //             {
    //                 title: 'Coleta de exames',
    //                 description: 'Resultado dos exames realizados'
    //             },
    //             {
    //                 title: 'Retorno exame cardiologista',
    //                 description: 'Consulta agendada para avaliar resultado do tratamento'
    //             },
    //             {
    //                 title: 'Solicitação de internação',
    //                 description: 'Internação agendada para acompanhamento e mais exames'
    //             }
    //         ]
    //     },
    //     {
    //         date: '02/02/2022',
    //         icon: HotelIcon,
    //         color: 'error',
    //         events: [
    //             {
    //                 title: 'Internação',
    //                 description: 'Apresentou problemas respiratórios necessitando de aparelhos'
    //             }
    //         ]
    //     }
    // ]);

    function groupBy(list, keyGetter) {
        const map = new Map();
        list.forEach((item) => {
            const key = keyGetter(item);
            const collection = map.get(key);
            if (!collection) {
                map.set(key, [item]);
            } else {
                collection.push(item);
            }
        });
        return map;
    }

    useEffect(() => {
        const newTimeline = [];
        if (events)
            events.map((item) => {
                const time = {
                    date: item.aprazamento.dataUltimoAtendimentoFormatadaIgao,
                    icon:
                        item.tipo === 'CONSULTA'
                            ? PersonSearch
                            : item.tipo === 'EXAME'
                            ? Science
                            : item.tipo === 'INTERNACAO'
                            ? HotelIcon
                            : Notifications,
                    color:
                        item.tipo === 'CONSULTA'
                            ? 'info'
                            : item.tipo === 'EXAME'
                            ? 'primary'
                            : item.tipo === 'INTERNACAO'
                            ? 'error'
                            : 'warning',
                    title: item.titulo,
                    description: item.descricao,
                    type: item.tipo
                };
                newTimeline.push(time);
                return;
            });

        setTimeline(newTimeline);
        // _events.map(item => {
        //     if (item.aprazamento?.dataUltimoAtendimentoFormatadaIgao) {
        //         if (timeline.find(time => time.date === item.aprazamento?.dataUltimoAtendimentoFormatadaIgao)) {

        //         }
        //     }
        //     const timeEvent = {
        //         date: item.aprazamento?.dataUltimoAtendimentoFormatadaIgao
        //     }
        // })
        //  const groupedEvents = groupBy(events, (events) => events.aprazamento.dataUltimoAtendimento);
    }, [events]);

    const handleSelectedItem = (item) => {
        setIsOpen(!isOpen);
        setSelected(item);
    };

    return (
        <>
            <Button variant="outlined" color="primary" onClick={() => setIsCadOpen(!isCadOpen)}>
                Novo Registro
            </Button>
            <Timeline position="alternate">
                {console.log(timeline)}
                {timeline.map((timeEvent, index) => (
                    <TimelineItem key={index}>
                        <TimelineOppositeContent sx={{ m: 'auto 0' }} align="right" variant="h4" color="primary.200">
                            {timeEvent.date}
                        </TimelineOppositeContent>
                        <TimelineSeparator>
                            <TimelineConnector />
                            <TimelineDot color={timeEvent.color}>
                                <timeEvent.icon />
                            </TimelineDot>
                            <TimelineConnector />
                        </TimelineSeparator>
                        <TimelineContent sx={{ py: '12px', px: 2 }}>
                            <div
                                key={index}
                                onClick={() => handleSelectedItem(timeEvent)}
                                style={{ cursor: 'pointer', marginBottom: '10px' }}
                            >
                                <Typography variant="h4" component="span">
                                    {timeEvent.title}
                                </Typography>
                                <Typography>{timeEvent.description}</Typography>
                            </div>
                        </TimelineContent>
                        {/* <TimelineContent sx={{ py: '12px', px: 2 }}>
                            {timeEvent.events.map((_event, index) => (
                                <div
                                    key={index}
                                    onClick={() => handleSelectedItem(_event)}
                                    style={{ cursor: 'pointer', marginBottom: '10px' }}
                                >
                                    <Typography variant="h6" component="span">
                                        {_event.title}
                                    </Typography>
                                    <Typography>{_event.description}</Typography>
                                </div>
                            ))}
                        </TimelineContent> */}
                    </TimelineItem>
                ))}
            </Timeline>

            <Dialog
                open={isOpen}
                onClose={() => setIsOpen(!isOpen)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{selected.title}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">{selected.description}</DialogContentText>
                    {selected.type === 'EXAME' && (
                        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <FileDownload />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary="Resultado" secondary="Jan 9, 2014" />
                            </ListItem>
                        </List>
                    )}
                </DialogContent>
            </Dialog>

            <Dialog
                open={isCadOpen}
                onClose={() => setIsCadOpen(!isCadOpen)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">O que deseja nos contar?</DialogTitle>
                <DialogContent>
                    <TextField
                        onChange={(event) => setNewEvent(event.target.value)}
                        value={newEvent}
                        label="Descrição"
                        fullWidth
                        margin="normal"
                        type="text"
                        sx={{ ...theme.typography.customInput }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={() => {
                            setTimeline((old) => [
                                ...old,
                                {
                                    title: newEvent,
                                    description: 'Novo evento inserido manual',
                                    icon: Notifications,
                                    color: 'warning',
                                    date: new Intl.DateTimeFormat('en-US').format(new Date())
                                }
                            ]);
                            setIsCadOpen(!isCadOpen);
                        }}
                    >
                        Salvar
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default BeneficiaryTimeline;
