import React, { useState, useEffect } from 'react';
import { Divider, Grid, TextField, Avatar, useMediaQuery } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import BeneficiaryTimeline from '../beneficiary-timeline';
import UaaS from 'services/uaas';
import { Person } from '@mui/icons-material';

const Beneficiary = ({ ...others }) => {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));

    const [beneficiary, setBeneficiary] = useState({});

    const loadBeneficiary = async () => {
        const response = await UaaS.getBeneficiary('57fa28ae-d443-4f34-9f78-156e21f166ed');
        setBeneficiary(response);
    };

    useEffect(() => {
        loadBeneficiary();
    }, []);

    const Text = ({ name, label, value }) => (
        <TextField
            fullWidth
            label={label}
            margin="normal"
            name={name}
            type="text"
            value={value}
            InputProps={{
                readOnly: true
            }}
            sx={{ ...theme.typography.customInput }}
        />
    );

    return (
        <>
            <Grid container spacing={matchDownSM ? 0 : 2} direction="column" alignItems="stretch">
                <Grid item alignSelf="center" sx={{ marginBottom: 2 }}>
                    <Avatar
                        alt="Remy Sharp"
                        //   src="/static/images/avatar/1.jpg"
                        sx={{ width: 120, height: 120, textAlign: 'center' }}
                    >
                        <Person fontSize="large" />
                    </Avatar>
                </Grid>
                <Divider>Dados Pessoais</Divider>
                <Grid container spacing={matchDownSM ? 0 : 2} marginLeft={0.05}>
                    <Grid item xs md>
                        <Text label="Nome" name="fname" value={beneficiary.nomeCompleto} />
                    </Grid>
                    <Grid item xs={3} md={2}>
                        <Text label="CPF" name="fname" value={beneficiary.cpf} />
                    </Grid>
                    <Grid item xs={4} md={3}>
                        <Text label="Nº Carteirinha" name="fname" value={beneficiary.carteirinha} />
                    </Grid>
                    <Grid item xs={4} md={3}>
                        <Text label="Tipo Usuário" name="fname" value={beneficiary.tipoUsuario} />
                    </Grid>
                </Grid>
                <Grid container spacing={matchDownSM ? 0 : 2} marginLeft={0.05}>
                    <Grid item xs md>
                        <Text
                            label="Nome"
                            name="fname"
                            value={`${beneficiary.endereco?.logradouro}, ${beneficiary.endereco?.numero}, ${beneficiary.endereco?.bairro}, ${beneficiary.endereco?.cep}, ${beneficiary.endereco?.estado}`}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={matchDownSM ? 0 : 2} marginLeft={0.05}>
                    <Grid item xs md>
                        {/* {console.log(Intl.DateTimeFormat('pt-BR').format(beneficiary.dataNascimento))} */}
                        <Text label="Data Nascimento" name="lname" value={beneficiary.dataNascimento} />
                    </Grid>
                    <Grid item xs md>
                        <Text label="Sexo" name="lname" value={beneficiary.sexo} />
                    </Grid>
                    <Grid item xs md>
                        <Text label="Estado Civil" name="lname" value={beneficiary.estadoCivil} />
                    </Grid>
                    {/* <Grid item xs md>
                        <Text label="Tipo Sanguíneo" name="lname" />
                    </Grid> */}
                </Grid>
                <Grid container spacing={matchDownSM ? 0 : 2} marginLeft={0.05}>
                    <Grid item xs md>
                        <Text label="E-mail" name="lname" value={beneficiary.contato?.email} />
                    </Grid>
                    <Grid item xs md>
                        <Text label="Telefone" name="lname" value={`${beneficiary.contato?.ddd} ${beneficiary.contato?.telefone}`} />
                    </Grid>
                    {/* <Grid item xs md>
                        <Text label="Celular" name="lname" defaultValue="" sx={{ ...theme.typography.customInput }} />
                    </Grid> */}
                </Grid>
            </Grid>
            <Divider>Perfil de Saúde</Divider>
            <Grid container spacing={matchDownSM ? 0 : 2} direction="column" alignItems="stretch" marginTop={0.05}>
                <Grid container spacing={matchDownSM ? 0 : 2} marginLeft={0.05}>
                    <Grid item xs md>
                        <Text label="Tipo Sanguineo" name="lname" value={beneficiary.perfilSaude?.tipoSanguineo} />
                    </Grid>
                    <Grid item xs md>
                        <Text label="Altura" name="lname" value={beneficiary.perfilSaude?.altura} />
                    </Grid>
                    <Grid item xs md>
                        <Text label="Peso" name="lname" value={beneficiary.perfilSaude?.peso} />
                    </Grid>
                    <Grid item xs md>
                        <Text label="IMC" name="lname" value={beneficiary.perfilSaude?.imc} />
                    </Grid>
                </Grid>
            </Grid>
            <Divider>Alergias</Divider>
            <Grid container spacing={matchDownSM ? 0 : 2} marginLeft={0.05}>
                <Grid item>Não possui</Grid>
            </Grid>
            <Divider>Doenças Crônicas</Divider>
            <Grid container spacing={matchDownSM ? 0 : 2} marginLeft={0.05}>
                <Grid item>Não possui</Grid>
            </Grid>
            <Divider>Diário de Saúde</Divider>
            <BeneficiaryTimeline events={beneficiary.historico} />
        </>
    );
};

export default Beneficiary;
